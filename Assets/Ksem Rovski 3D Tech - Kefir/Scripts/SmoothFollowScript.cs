﻿using UnityEngine;
using System.Collections;

public class SmoothFollowScript : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private Transform TargetPivotRotation;

    [SerializeField]
    private float CameraAngle = 15;

    [SerializeField]
    private float smooth = 5.0f;

    void Update()
    {
        //Smooth position follow
        transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * smooth);

        //Keep X camera angle, rotate Y based on Y of character
        var yRotation = TargetPivotRotation.transform.rotation.eulerAngles.y;
        gameObject.transform.rotation = Quaternion.Euler(CameraAngle, yRotation, 0);
    }
}