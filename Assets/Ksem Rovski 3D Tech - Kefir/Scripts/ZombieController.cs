﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ZombieController : MonoBehaviour
{
    //For pressed buttons visual debug purposes
    public GameObject ButtonW;
    public GameObject ButtonA;
    public GameObject ButtonS;
    public GameObject ButtonD;
    public GameObject ButtonE;
    public GameObject ButtonLMB;
    public GameObject ButtonRMB;

    //Link to Lose Panel that activates after ragdolling
    public GameObject DeathPanel;

    //Array of kinematic bodyparts, is required to active ragdoll mechanic
    public Rigidbody[] BodyParts;

    void Update()
    {
        //Run forward or stop running
        if (Input.GetKeyDown("w"))
        {
            ButtonW.SetActive(true);
            this.GetComponent<Animator>().SetFloat("Blend", 1);
            this.GetComponent<Animator>().SetFloat("Speed", 1);
        }
        else if (Input.GetKeyUp("w"))
        {
            ButtonW.SetActive(false);
            this.GetComponent<Animator>().SetFloat("Blend", 0);
        }

        //Run backwards
        if (Input.GetKeyDown("s"))
        {
            ButtonS.SetActive(true);
            this.GetComponent<Animator>().SetFloat("Blend", 1);
            this.GetComponent<Animator>().SetFloat("Speed", -1);
        }
        else if (Input.GetKeyUp("s"))
        {
            ButtonS.SetActive(false);
            this.GetComponent<Animator>().SetFloat("Blend", 0);
            this.GetComponent<Animator>().SetFloat("Speed", 1);
        }

        //Turn left and right
        if (Input.GetKey("a"))
        {
            transform.Rotate(0, -2f, 0);
            ButtonA.SetActive(true);
        }else if (Input.GetKeyUp("a")) ButtonA.SetActive(false);
        if (Input.GetKey("d"))
        {
            transform.Rotate(0, 2f, 0);
            ButtonD.SetActive(true);
        }else if (Input.GetKeyUp("d")) ButtonD.SetActive(false);

        //Faster running
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            this.GetComponent<Animator>().SetFloat("Speed", 2);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift)) this.GetComponent<Animator>().SetFloat("Speed", 1);

        //Restart Scene
        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("ZombieTPS");
        }
        //Quit game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        //Meme (just testing Umotion for zombie humanoid rig)
        if (Input.GetKeyDown("q"))
        {
            this.GetComponent<Animator>().SetTrigger("Meme");
        }
        
        //Turn zombie into ragdoll and show/hide pressed button
        if (Input.GetKeyDown("e"))
        {
            ButtonE.SetActive(true);
            Ragdoller();
        }
        else if (Input.GetKeyUp("e")) ButtonE.SetActive(false);
   
        //Detect left mouse button click
        if (Input.GetMouseButtonDown(0))
        {
            //Playing slightly random zombie growl
            this.GetComponent<AudioSource>().pitch = Random.RandomRange(0.75f, 1.15f);
            this.GetComponent<AudioSource>().Play();

            //Add attack animation randomizer
            switch (Random.Range(0, 2))
            {
                case 1:
                    ButtonLMB.SetActive(true);
                    this.GetComponent<Animator>().SetTrigger("Attack1");
                    break;
                case 0:
                    ButtonLMB.SetActive(true);
                    this.GetComponent<Animator>().SetTrigger("Attack2");
                    break;
            }
        }
        else if (Input.GetMouseButtonUp(0)) ButtonLMB.SetActive(false);
    }

    private void Ragdoller()
    {
        //YOU LOSE PANEL, press "R" to restart
        DeathPanel.SetActive(true);

        //Turn off animator, loop deactive Kinematic on all body parts
        this.GetComponent<Animator>().enabled = false;
        for (var i = 0; i < BodyParts.Length; i++)
        {
            BodyParts[i].isKinematic = false;
        }
    }

    //Check hammer that kills character on collision
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "KILLER")
        {
            Ragdoller();
        }
     }
}
