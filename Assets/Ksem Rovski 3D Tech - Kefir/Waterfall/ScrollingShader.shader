        Shader "ScrollingShader" {
            Properties {
				_Color("Main Color", Color) = (1,1,1,1)
				_Cutoff("Cutoff", Range(0,1)) = 0.5
                _MainTex ( "Base (RGB)", 2D ) = "white" {}
                _XScrollSpeed ( "X Scroll Speed", Float ) = 1
                _YScrollSpeed ( "Y Scroll Speed", Float ) = 1
            }
            SubShader {
				Tags { "Queue" = "Transparent" "RenderType" = "TransparentCutout" "IgnoreProjector" = "True" }
                LOD 200
			    
				Lighting Off
                CGPROGRAM
				#pragma surface surf Lambert

         
                sampler2D _MainTex;
				fixed4 _Color;        
				float _Cutoff;
                float _XScrollSpeed;
                float _YScrollSpeed;
         
                struct Input {
                    float2 uv_MainTex;
                };
         
                void surf( Input IN, inout SurfaceOutput o ) {
                    fixed2 scrollUV = IN.uv_MainTex;
                    fixed xScrollValue = _XScrollSpeed * _Time.x;
                    fixed yScrollValue = _YScrollSpeed * _Time.x;
                    scrollUV += fixed2( xScrollValue, yScrollValue );
                    half4 c = tex2D( _MainTex, scrollUV ) * _Color;
                    o.Albedo = c.rgb;
                    o.Alpha = c.a;             
					clip(c.a - _Cutoff);
                }
                ENDCG
            }
            FallBack "Diffuse"
        }

     

